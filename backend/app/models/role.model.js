const mongoose = require("mongoose");

const Role = mongoose.model(
    "Role",
    new mongoose.Schema(
        {
            name: String
        },
        {
            timestamps: {
                createdAt: "created_at",
                updatedAt: "updated_at"
            }
        }
    )
);

exports.Role = Role;