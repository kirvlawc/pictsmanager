const mongoose = require("mongoose");
const { Role } = require('./role.model')

const User = mongoose.model(
    "User",
    new mongoose.Schema(
        {
            username: {
                type: String,
                unique: true
            },
            email: {
                type: String,
                unique: true
            },
            password: String,
            roles: [{
                type: mongoose.Schema.Types.ObjectId,
                ref: "Role"
            }]
        },
        {
            timestamps: {
                createdAt: "created_at",
                updatedAt: "updated_at"
            }
        }
    )
);

exports.User = User;