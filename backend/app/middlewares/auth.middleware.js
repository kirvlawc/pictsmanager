const authConfig = require("../configs/auth.config");
const db = require("../models");
const jwt = require("jsonwebtoken");

const { Role, ROLES, User} = db;
const { TokenExpiredError } = jwt;

/**
 * Verifies whether the provided JWT token is still valid or has expired.
 * @param {object} req Object representing the HTTP request containing all query properties and parameters
 * @param {object} res Object representing the HTTP response that the app sends back when it get and HTTP request
 * @param {function} next Callback function to be called when the function has completed
 */
const verifyToken = (req, res, next) => {
    var token = req.headers["x-access-token"];

    if (!token) {
        return res.status(403).send({ message: "No token provided!" });
    }

    jwt.verify(token, authConfig.secret, (err, decoded) => {
        if (err) {
            if (err instanceof TokenExpiredError) {
                return res.status(401).send({ message: "Unauthorized! Access token has expired!" });
            } else {
                return res.status(401).send({ message: "Unauthorized!" });
            }
        }

        req.userId = decoded.id;
        next();
    });
};

/**
 * Verifies whether the user associated to the provided JWT token has administrator rights.
 * @param {object} req Object representing the HTTP request containing all query properties and parameters
 * @param {object} res Object representing the HTTP response that the app sends back when it get and HTTP request
 * @param {function} next Callback function to be called when the function has completed
 */
const isAdmin = (req, res, next) => {
    User.findById(req.userId)
        .exec((err, user) => {
            if (err) {
                res.status(500).send({ message: err });
                return;
            }

            Role.find({ _id: { $in: user.roles }}, (err, roles) => {
                if (err) {
                    res.status(500).send({ message: err });
                    return;
                }

                for (let i = 0; i < roles.length; i++) {
                    if (roles[i].name === "admin") {
                        next();
                        return;
                    }
                }

                res.status(403).send({ message: "Forbidden!" });
                return;
            });
    });
};

/**
 * Verifies whether the user associated to the provided JWT token has moderator rights.
 * @param {object} req Object representing the HTTP request containing all query properties and parameters
 * @param {object} res Object representing the HTTP response that the app sends back when it get and HTTP request
 * @param {function} next Callback function to be called when the function has completed
 */
const isModerator = (req, res, next) => {
    User.findById(req.userId)
        .exec((err, user) => {
            if (err) {
                res.status(500).send({ message: err });
                return;
            }

            Role.find({ _id: { $in: user.roles }},(err, roles) => {
                if (err) {
                    res.status(500).send({ message: err });
                    return;
                }

                for (let i = 0; i < roles.length; i++) {
                    if (roles[i].name === "moderator") {
                        next();
                        return;
                    }
                }

                res.status(403).send({ message: "Forbidden!" });
                return;
            });
    });
};

/**
 * Verifies whether the provided email and username in the request body have already been taken.
 * @param {object} req Object representing the HTTP request containing all query properties and parameters
 * @param {object} res Object representing the HTTP response that the app sends back when it get and HTTP request
 * @param {function} next Callback function to be called when the function has completed
 */
const checkDuplicateUser = (req, res, next) => {
    User.findOne({ username: req.body.username })
        .exec((err, user) => {
            if (err) {
                res.status(500).send({ message: err });
                return;
            }

            if (user) {
                res.status(400).send({ message: "Failed! Username is already in use!" });
                return;
            }

            User.findOne({ email: req.body.email })
                .exec((err, user) => {
                    if (err) {
                        res.status(500).send({ message: err });
                        return;
                    }

                    if (user) {
                        res.status(400).send({ message: "Failed! Email is already in use!" });
                        return;
                    }

                    next();
                });
        });
};

/**
 * Verifies whether the roles provided in the request body actually exist.
 * @param {object} req Object representing the HTTP request containing all query properties and parameters
 * @param {object} res Object representing the HTTP response that the app sends back when it get and HTTP request
 * @param {function} next Callback function to be called when the function has completed
 */
const checkRolesExist = (req, res, next) => {
    if (req.body.roles && Array.isArray(req.body.roles) && req.body.roles.length) {
        for (let i = 0; i < req.body.roles.length; i++) {
            if (!ROLES.includes(req.body.roles[i])) {
                res.status(400).send({ message: `Failed! Role '${req.body.roles[i]}' does not exist!` });
                return;
            }
        }
    }

    next();
};


module.exports = {
    checkDuplicateUser,
    checkRolesExist,
    isAdmin,
    isModerator,
    verifyToken
};