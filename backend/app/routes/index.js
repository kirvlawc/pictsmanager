const { albumRoutes } = require('./album.routes');
const { authRoutes } = require('./auth.routes');
const { imageRoutes } = require('./image.routes');

module.exports = {
    albumRoutes,
    authRoutes,
    imageRoutes
}