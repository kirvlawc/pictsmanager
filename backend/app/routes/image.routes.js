const express = require('express');
const { authMiddleware } = require('../middlewares');
const { imageController } = require('../controllers');
const router = express.Router();

router.get('/', [authMiddleware.verifyToken], imageController.getImages.bind(imageController));

router.post('/', [authMiddleware.verifyToken], imageController.upload.bind(imageController));

router.post('/single', [authMiddleware.verifyToken], imageController.uploadSingle.bind(imageController));

router.get('/view/:filename', imageController.viewImage.bind(imageController));

router.get('/:id', [authMiddleware.verifyToken], imageController.getImage.bind(imageController));

router.patch('/:id', [authMiddleware.verifyToken], imageController.update.bind(imageController));

router.delete('/:id', [authMiddleware.verifyToken], imageController.delete.bind(imageController));

exports.imageRoutes = router;