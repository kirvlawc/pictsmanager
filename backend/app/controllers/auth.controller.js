const authConfig = require("../configs/auth.config");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const { RefreshToken, Role, User } = require("../models");

class AuthController {

    /**
     * Check whether a username already exists in the database
     * @param {object} req Object representing the HTTP request containing all query properties and parameters
     * @param {object} res Object representing the HTTP response that the app sends back when it get and HTTP request
     */
    usernameExists(req, res) {
        User.findOne({ username: req.body.username })
            .exec((err, user) => {
                if (err) {
                    console.log(err);
                    return res.status(500).send({ message: "An unexpected error occured." });
                }

                if (user) {
                    return res.send({ usernameExists: true });
                }

                return res.send({ usernameExists: false });
            });
    }

    /**
     * Check whether an email already exists in the database
     * @param {object} req Object representing the HTTP request containing all query properties and parameters
     * @param {object} res Object representing the HTTP response that the app sends back when it get and HTTP request
     */
    emailExists(req, res) {
        User.findOne({ email: req.body.email })
            .exec((err, user) => {
                if (err) {
                    console.log(err);
                    return res.status(500).send({ message: "An unexpected error occured." });
                }

                if (user) {
                    return res.send({ emailExists: true });
                }

                return res.send({ emailExists: false });
            });
    }

    /**
     * Adds a new user to the database and assigns them a role.
     * @param {object} req Object representing the HTTP request containing all query properties and parameters
     * @param {object} res Object representing the HTTP response that the app sends back when it get and HTTP request
     */
    signup(req, res) {
        const newUser = this.parse(req.body);

        newUser.save((err, user) => {
            if (err)
                throw err;

            if (req.body.roles) {
                Role.find({ name: { $in: req.body.roles } }, (err, roles) => {
                    if (err)
                        throw err;

                    user.roles = roles.map(role => role._id);
                    user.save(err => {
                        if (err) {
                            console.log(err);
                            res.send({ message: "Unable to save the user provided." });
                        }

                        res.send({ message: "User was registered successfully!" });
                    });
                });
            } else {
                Role.findOne({ name: "user" }, (err, role) => {
                    if (err) {
                        console.log(err);
                        return res.send({ message: "Unable to save the user provided." });
                    }

                    user.roles = [role._id];
                    user.save(err => {
                        if (err)
                            throw err;

                        return res.send({ message: "User was registered successfully!" });
                    });
                });
            }
        });
    }

    /**
     * Logs in a user given their username and password. Returns a user object along with roles and access tokens.
     * @param {object} req Object representing the HTTP request containing all query properties and parameters
     * @param {object} res Object representing the HTTP response that the app sends back when it get and HTTP request
     */
    signin(req, res) {
        User.findOne({ username: req.body.username })
            .populate("roles", "-__v")
            .exec(async (err, user) => {
                if (err) {
                    console.log(err);
                    return res.status(500).send({ message: "An unexpected error occured." });
                }

                if (!user) {
                    return res.status(404).send({ message: "User Not found." });
                }

                var passwordIsValid = bcrypt.compareSync(req.body.password, user.password);

                if (!passwordIsValid) {
                    return res.status(401).send({
                        accessToken: null,
                        message: "Invalid Password!"
                    });
                }

                var token = jwt.sign({ id: user.id }, authConfig.secret, {
                    expiresIn: authConfig.jwtExpiration
                });

                var refreshToken = await RefreshToken.createToken(user);

                var authorities = [];

                for (let i = 0; i < user.roles.length; i++) {
                    authorities.push("ROLE_" + user.roles[i].name.toUpperCase());
                }

                return res.status(200).send({
                    id: user._id,
                    username: user.username,
                    email: user.email,
                    roles: authorities,
                    accessToken: token,
                    refreshToken: refreshToken
                });
            });
    }

    /**
     * Refreshes a users access token given their refreshToken.
     * @param {object} req Object representing the HTTP request containing all query properties and parameters
     * @param {object} res Object representing the HTTP response that the app sends back when it get and HTTP request
     */
    refreshToken(req, res) {
        const { refreshToken: requestToken } = req.body;

        if (!requestToken) {
            return res.status(403).json({ message: "Refresh Token is required!" });
        }

        RefreshToken.findOne({ token: requestToken })
            .exec((err, refreshToken) => {
                if (err) {
                    console.log(err)
                    return res.status(500).send({ message: "An unexpected error occured." });
                }

                if (!refreshToken) {
                    return res.status(404).json({ message: "Refresh token not found!" });
                }

                if (RefreshToken.verifyExpiration(refreshToken)) {
                    RefreshToken.findByIdAndRemove(refreshToken._id, { useFindAndModify: false }).exec();

                    res.status(403).json({ message: "Refresh token has expired. Please sign in again." });
                    return;
                }

                let newAccessToken = jwt.sign({ id: refreshToken.user._id }, authConfig.secret, {
                    expiresIn: authConfig.jwtExpiration,
                });

                return res.status(200).json({
                    accessToken: newAccessToken,
                    refreshToken: refreshToken.token,
                });
            });
    };

    /**
     * Parse a JSON object into a new `User` object
     * @param {object} userData JSON object containing `User` properties
     * @returns a new `User`
     */
    parse(userData) {
        const { username, email, password } = userData;

        return new User({
            username: username,
            email: email,
            password: bcrypt.hashSync(password)
        });
    }

}

exports.authController = new AuthController();