//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

const mongoose = require('mongoose');

//Require the dev-dependencies
const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../../index');
const should = chai.should();

const { RefreshToken, Role, User } = require("../models");


chai.use(chaiHttp);

describe('Authentication', () => {
    // Before each test we empty the database
    beforeEach((done) => {
        User.deleteMany({}, (err) => {
            done();
        });
    });

    describe('POST /api/auth/signup', () => {
        it('It should successfully register a user.', (done) => {
            const user = {
                user: "test",
                email: "test@test.com",
                password: "test"
            };

            chai.request(server)
                .post('/api/auth/signup')
                .send(user)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('message');
                    res.body.should.have.property('message').eql('User was registered successfully!');
                    done();
                });
        });
    });

    describe('POST /api/auth/signin', () => {
        it('It should not successfully login an unknown user.', (done) => {
            const login = {
                username: "test",
                password: "test"
            };

            chai.request(server)
                .post('/api/auth/signin')
                .send(login)
                .end((err, res) => {
                    res.should.have.status(404);
                    res.body.should.be.a('object');
                    res.body.should.have.property('message');
                    res.body.should.have.property('message').eql('User Not found.');
                    done();
                });
        });
    });

    describe('POST /api/auth/signin', () => {
        it('It should not successfully login a user with a wrong password.', (done) => {
            const user = {
                user: "test",
                email: "test@test.com",
                password: "test"
            };

            const login = {
                username: "test",
                password: "wrong_password"
            };

            chai.request(server)
                .post('/api/auth/signup')
                .send(user)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('message');
                    res.body.should.have.property('message').eql('User was registered successfully!');
                });

            chai.request(server)
                .post('/api/auth/signin')
                .send(login)
                .end((err, res) => {
                    res.should.have.status(401);
                    res.body.should.be.a('object');
                    res.body.should.have.property('message');
                    res.body.should.have.property('message').eql('Invalid Password!');
                    done();
                });
        });
    });

});
