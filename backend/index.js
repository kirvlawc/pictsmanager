const cors = require('cors');
const dbConfig = require('./app/configs/db.config');
const express = require('express');
const routes = require('./app/routes');

const { initializeDB } = require('./app/utils/initialize-db');
const { mongoose } = require('./app/models');

const app = express();

var corsOptions = {
    origin: [
        'https://<MY-APP-ID>.firebaseapp.com',
        `http://localhost:${process.env.SERVER_PORT}`,
        'http://localhost:8080'
    ]
};

app.use(cors(corsOptions));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(function (req, res, next) {
    res.header(
        "Access-Control-Allow-Headers",
        "x-access-token, Origin, Content-Type, Accept"
    );
    next();
});

// Database connection
mongoose
    .connect(`mongodb://${dbConfig.DB_USER}:${dbConfig.DB_PASS}@${dbConfig.DB_HOST}:${dbConfig.DB_PORT}/${dbConfig.DB}?authSource=admin&w=1`, {
        useCreateIndex: true,
        useFindAndModify: false,
        useNewUrlParser: true,
        useUnifiedTopology: true
    }).then(() => {
        console.log("Successfully connect to MongoDB.");
        initializeDB();
    }).catch(err => {
        console.error("Connection error", err);
        process.exit();
    });


// Route Setup
app.use('/api/album', routes.albumRoutes);
app.use('/api/auth', routes.authRoutes);
app.use('/api/images', routes.imageRoutes);


const PORT = 8080;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}.`);
});